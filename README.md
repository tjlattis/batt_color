# batt_color

batt_color queries the estimated remaining battery percentage on an OSX machine and references a provided 256color spectrum to provide a corresponding color. It can be called by most configuration files that can execute code. I use it to set the background color of the battery indicator section of my Tmux status bar, giving a quick visual reference for battery state. But it could be used for any other configurable cli program. 

# Installation

NOTE: currently only works for MacOS

Simply copy set_batt_color.sh to wherever you keep executable sh files and change the path in line 5 to point toward your spectrum. Two spectra are included. One with slightly lowered saturation to fit more muted color schemes. 

# License

(C) Anthony Lattis 2019

Distributed under [WTFPL](http://www.wtfpl.net/about/) 
Version 2, December 2004 

Everyone is permitted to copy and distribute verbatim or modified copies of this license document, and changing it is allowed as long as the name is changed. 
