#! /usr/local/bin/bash

n=$(pmset -g batt | sed -n 2p | cut -f2 -d$'\t' | cut -f1 -d';')

readarray -t col < /PATH/TO/SECTRUM/FILE # CHANGE THIS LINE 

bg=${col[${n::-1}]}

echo $bg
